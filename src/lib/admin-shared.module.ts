import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { environment } from './environments/environment';
import { ADMIN_LOCALES } from './i18n';
import { AdminService } from './admin.service';
import { PermissionsComponent } from './permissions/permissions.component';
import { TablesModule} from '@universis/ngx-tables';
import { AdvancedFormsModule } from '@universis/forms';
import {UsersListConfigurationResolver, UsersListSearchResolver} from './users/users-list/users-list-config.resolver';

@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
        TablesModule,
        AdvancedFormsModule
    ],
    declarations: [
      PermissionsComponent,
    ],
    exports: [
      PermissionsComponent
    ],
    providers: [
      AdminService,
      UsersListConfigurationResolver,
      UsersListSearchResolver
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
  })
  export class AdminSharedModule {
    constructor( @Optional() @SkipSelf() parentModule: AdminSharedModule, private translateService: TranslateService) {
      this.ngOnInit();
    }

    static forRoot(): ModuleWithProviders<AdminSharedModule> {
      return {
        ngModule: AdminSharedModule,
        providers: [
          AdminService
        ]
      };
    }
    // tslint:disable-next-line:use-life-cycle-interface use-lifecycle-interface
    ngOnInit() {
      environment.languages.forEach( (language) => {
        if (ADMIN_LOCALES.hasOwnProperty(language)) {
          this.translateService.setTranslation(language, ADMIN_LOCALES[language], true);
        }
      });
    }

  }
