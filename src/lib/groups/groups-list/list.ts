/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const group_list = {
  "title": "Admin.Groups.All",
  "model": "Groups",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "selectable": false,
  "multipleSelect": true,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["../../../groups", "${id}", "edit"]
      }
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Groups.Name"
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Groups.AlternateName"
    },
    {
      "name": "description",
      "property": "description",
      "title": "Admin.Users.Description"
    }
  ],
  "criteria": [
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ]
};
