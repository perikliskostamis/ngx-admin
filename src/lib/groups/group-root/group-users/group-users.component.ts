import {Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {
  ActivatedTableService, AdvancedRowActionComponent,
  AdvancedTableComponent, AdvancedSelectService,
  AdvancedTableConfiguration, AdvancedTableDataResult
} from '@universis/ngx-tables';

import {users_list}  from './users';
import {AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ClientDataQueryable} from '@themost/client';


@Component({
  selector: 'lib-group-users',
  templateUrl: './group-users.component.html'
})
export class GroupUsersComponent implements AfterViewInit, OnDestroy {

  public group: any;
  public groupId: any;
  public usersToBeInserted = 0;
  @ViewChild('members') members: AdvancedTableComponent;
  public filter: any = {};

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService,
              private _loadingService: LoadingService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _selectService: AdvancedSelectService,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService
  ) {
  }


  public recordsTotal: any;
  public description;
  any;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private selectedItems = [];
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  ngAfterViewInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.groupId = this._activatedRoute.parent.snapshot.params.id;
    }
    this.group = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'Group') {
        // reload request
        this.recordsTotal = 0;
        // tslint:disable-next-line:triple-equals
        if (event.target.id == this.groupId) {
          this.group = event.target;
          this.members.config = AdvancedTableConfiguration.cast(users_list);
          this.members.config.model = `Groups/${this.group.id}/members`;
          this.members.fetch();
        } else {
          // load group
          this.group = this._context.model('Groups').where('id').equal(event.target.id).getItem().then(result => {
            this.group = result;
            this.members.config = AdvancedTableConfiguration.cast(users_list);
            this.members.config.model = `Groups/${this.group.id}/members`;
            this.members.fetch();
          });
        }
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.members && this.members.lastQuery) {
      const lastQuery: ClientDataQueryable = this.members.lastQuery;
      if (lastQuery != null) {
        if (this.members.smartSelect) {
          // get items
          const selectArguments = ['id', 'name', 'alternateName'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.members.unselected && this.members.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.members.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.members.selected.map((item) => {
            return {
              id: item.id,
              name: item.name,
              alternateName: item.alternateName

            };
          });
        }
      }
    }
    return items;
  }

  async removeUser() {
    let items = await this.getSelectedItems();
    if (items && items.length) {
      const group = {id: parseInt(this.groupId, 10), $state: 4};
      items = items.map(item => {
        item.groups = [group];
        return item;
      });
      // get confirmation.
      this._modalService.showWarningDialog(
        this._translateService.instant('Admin.Groups.RemoveUsers.Title'),
        this._translateService.instant('Admin.Groups.RemoveUsers.Message'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        if (result === 'ok') {
          // save-remove items.
          this._context.model('Users').save(items).then(() => {
            // inform user.
            this._toastService.show(
              this._translateService.instant('Admin.Groups.RemoveUsers.Title'),
              this._translateService.instant((items.length === 1 ?
                'Admin.Groups.RemoveUsers.One' : 'Admin.Groups.RemoveUsers.Many')
                , {value: items.length})
            );
            // fetch users.
            this.members.fetch();
          }).catch(err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });
    }
  }

  async addUser() {
    this._selectService.select({
      modalTitle: 'Admin.Users.SelectUsers',
      tableConfigSrc: './assets/lists/Users/select.json'
    }).then(async (result) => {
      if (result.result === 'ok') {
        const selected = result.items;
        let items = [];
        if (selected && selected.length > 0) {
          items = selected.map(async (user) => {
            // get user id.
            const selectedUserId = user.id;
            // get selected user.
            const selectedUser = await this._context
              .model('Users')
              .where('id')
              .equal(selectedUserId)
              .expand('groups')
              .getItem();
            // get user groups.
            const userGroups = selectedUser.groups;
            // find group index.
            const groupIndex = userGroups.findIndex(
              (someGroup) => someGroup.id === this.groupId
            );
            // if the group does not already exist, a new user is to be added.
            if (groupIndex < 0) {
              this.usersToBeInserted++;
            }
            const group = {id: this.groupId};
            return {
              id: user.id,
              groups: [group]
            };
          });

          // execute promises and get users to be saved.
          const usersToBeSaved = await Promise.all(items);
          // if there are no new users to be inserted, inform and close.
          if (this.usersToBeInserted === 0) {
            this._toastService.show(
              this._translateService.instant('Admin.Groups.AddUsers.Title'),
              this._translateService.instant(
                'Admin.Groups.AddUsers.AllAlreadyExist'
              )
            );
            return null;
          }

          // save users.
          return this._context
            .model('Users')
            .save(usersToBeSaved)
            .then(() => {
              this._toastService.show(
                this._translateService.instant('Admin.Groups.AddUsers.Title'),
                this._translateService.instant(
                  this.usersToBeInserted === 1
                    ? 'Admin.Groups.AddUsers.One'
                    : 'Admin.Groups.AddUsers.Many',
                  {value: this.usersToBeInserted}
                )
              );
              // refresh the table
              this.members.fetch(true);
            })
            .catch((err) => {
              this._errorService.showError(err, {
                continueLink: '.',
              });
            });
        }
      }
    });
  }
}
