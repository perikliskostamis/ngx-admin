import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { AdminComponent } from './admin.component';
import { TablesModule } from '@universis/ngx-tables';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AdminRoutingModule } from './admin-routing.module';
import {TranslateModule} from '@ngx-translate/core';
import { UsersComponent } from './users/users.component';
import {UserRootComponent} from './users/user-root/user-root.component';
import {CommonModule} from '@angular/common';
import { UserGroupsComponent } from './users/user-root/user-groups/user-groups.component';
import { UserDepartmentsComponent } from './users/user-root/user-departments/user-departments.component';
import { UserEditComponent } from './users/user-root/user-edit/user-edit.component';
import {UserPreviewComponent} from './users/user-root/user-edit/user-preview/user-preview.component';
import {AdvancedFormsModule} from '@universis/forms';
import { GroupsComponent } from './groups/groups.component';
import { GroupsListComponent } from './groups/groups-list/groups-list.component';
import { GroupRootComponent } from './groups/group-root/group-root.component';
import { GroupEditComponent } from './groups/group-root/group-edit/group-edit.component';
import { GroupUsersComponent } from './groups/group-root/group-users/group-users.component';
import { AdminSharedModule } from './admin-shared.module';
import {SharedModule} from '@universis/common';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {RouterModalModule} from '@universis/common/routing';
import {ModalModule} from 'ngx-bootstrap/modal';
import {MostModule} from '@themost/angular';
@NgModule({
  imports: [
    TablesModule,
    AdminRoutingModule,
    TranslateModule,
    CommonModule,
    AdvancedFormsModule.forRoot(),
    MostModule,
    AdminSharedModule,
    SharedModule,
    TooltipModule,
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    RouterModalModule

  ],
  declarations: [
    AdminComponent,
    UsersListComponent,
    UserRootComponent,
    UsersComponent,
    UserGroupsComponent,
    UserDepartmentsComponent,
    UserEditComponent,
    UserPreviewComponent,
    GroupsComponent,
    GroupsListComponent,
    GroupRootComponent,
    GroupEditComponent,
    GroupUsersComponent
  ],
  exports: [AdminComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdminModule {
  //
}
