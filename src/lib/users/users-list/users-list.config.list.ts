/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const users_list = {
  "title": "Users",
  "model": "Users",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": ["../..", "${id}", "edit"]
      }
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Users.AlternateName"
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Users.Name"
    },
    {
      "name": "description",
      "property": "description",
      "title": "Admin.Users.Description"
    },
    {
      "name": "enabled",
      "property": "enabled",
      "title": "Admin.Users.StatusTitle",
      "formatter": "TranslationFormatter",
      "formatString": "Admin.Users.Enabled.${value}"
    },
    {
      "name": "groups",
      "property": "groups",
      "virtual": true,
      "sortable": false,
      "title": "Admin.Groups.TitlePlural",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${groups.length ? groups.map(x => x.name).join(', ') : '-'}"
        },
        {
          "formatter": "TranslationFormatter",
          "formatString": "${value}"
        }
      ]
    }
  ],
  "defaults": {
    "expand": "groups"
  },
  "criteria": [
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "enabled",
      "filter": "(enabled eq '${value}')",
      "type": "text"
    },
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "paths" : [
    {
      "name": "Users.Enabled.Plural",
      "alternateName": "list/active",
      "show": true,
      "filter": {
        "enabled": "1"
      }
    },
    {
      "name": "Users.All",
      "show": true,
      "alternateName": "list/index",
      "filter": { }
    }

  ],
  "searches": []
};
