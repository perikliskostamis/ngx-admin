/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const users_search = {
    "components": [
      {
        "label": "Columns",
        "columns": [
          {
            "components": [
              {
                "label": "Admin.Users.Name",
                "spellcheck": true,
                "tableView": true,
                "validate": {
                  "unique": false,
                  "multiple": false
                },
                "key": "name",
                "type": "textfield",
                "input": true,
                "hideOnChildrenHidden": false
              }
            ],
            "width": 6,
            "offset": 0,
            "push": 0,
            "pull": 0
          },
          {
            "components": [
              {
                "label": "Admin.Users.Enabled.1",
                "widget": "choicesjs",
                "data": {
                    "values": [
                        {
                            "label": "No",
                            "value": "0"
                        },
                        {
                            "label": "Yes",
                            "value": "1"
                        }
                    ]
                },
                "selectThreshold": 0.3,
                "validate": {
                    "unique": false,
                    "multiple": false
                },
                "key": "enabled",
                "valueProperty": "value",
                "selectValues": "value",
                "type": "select",
                "input": true,
                "searchEnabled": false,
                "hideOnChildrenHidden": false,
                "disableLimit": false,
                "lazyLoad": false
              }
            ],
            "width": 6,
            "offset": 0,
            "push": 0,
            "pull": 0
          }
        ],
        "tableView": false,
        "key": "enabled",
        "type": "columns",
        "input": false,
        "path": "columns"
      },
      {
        "components": [
          {
            "label": "Admin.Users.AlternateName",
            "spellcheck": true,
            "tableView": true,
            "validate": {
              "unique": false,
              "multiple": false
            },
            "key": "alternateName",
            "type": "textfield",
            "input": true,
            "hideOnChildrenHidden": false
          }
        ],
        "width": 6,
        "offset": 0,
        "push": 0,
        "pull": 0
      }

    ],
  "criteria" : [],
  "searches": [],
  "model": "",
  "defaults": [],
  "columns": []
  };
