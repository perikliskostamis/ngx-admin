import {Component, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AppEventService} from "@universis/common";
import { Subscription } from 'rxjs';

@Component({
  selector: 'lib-user-root',
  templateUrl: './user-root.component.html'
})
export class UserRootComponent implements OnInit, OnDestroy {
  public user: any;
  public isCreate = false;
  public config: any;
  public edit: any;
  private changeSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) { }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.params.id) {
      this.loadData();
    }
    this.changeSubscription = this._appEvent.change.subscribe(event => {
      if (event && event.model === 'Users' && this._activatedRoute.snapshot.params.id) {
        this.loadData();
      }
    });
  }

  loadData() {
    this._context.model('users')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem()
      .then(user => {
        this.user = user;
        this._appEvent.change.next({
          model: 'User',
          target: this.user
        });
      });
  }

  ngOnDestroy() {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
