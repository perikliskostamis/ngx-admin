import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { DiagnosticsService } from '@universis/common';
import { User, ObjectState, PermissionValidationObject, PermissionValidationResult, PermissionValidationOptions } from './admin.interfaces';

@Injectable()
export class AdminService {

  private permissionMasks = {
    READ: 1,
    CREATE: 2,
    UPDATE: 4,
    DELETE: 8,
    EXECUTE: 16
  };

  constructor(private context: AngularDataContext,
              private diagnostics: DiagnosticsService) {
   }

   createUser(user: User): Promise<User> {
     // forcibly set user for insert
     Object.assign(user, <ObjectState> {
       $state: 1
     });
    return this.context.model('Users').save(user);
   }

   updateUser(user: User): Promise<User> {
     // forcibly set user for update
     Object.assign(user, <ObjectState> {
       $state: 2
     });
    return this.context.model('Users').save(user);
   }

   getUser(username: string): Promise<User> {
    return this.context.model('Users').where('name').equal(username).getItem();
   }

   getPrivilegedAccounts(privilege: string, target: number | string): Promise<any> {
     return this.context.model('Users/Permissions')
        .where('privilege').equal(privilege)
        .and('target').equal(target)
        .expand('account')
        .getItems();
   }

   decodeMasksToPermissions(privilegedAccounts: any, maskMap: any, permissionsList: string[]) {
    for (let i = 0 ; i < privilegedAccounts.length; i++) {
      const account = privilegedAccounts[i];
      // assign permission fields to account.
      for (let k = 0; k < permissionsList.length; k++) {
        const permission = permissionsList[k];
        account[permission] = false;
      }
      if (account.mask === 0) {
        continue;
      }
      // try to find exact mask value.
      const findMask = maskMap.masks.findIndex(mask => mask === account.mask);
      if (findMask > -1) {
        // set permission to true.
        account[maskMap.permissions[findMask]] = true;
        // continue.
        continue;
      }
      // decode account mask.
      this.maskDecoder(maskMap.masks, account.mask, account, maskMap, []);
    }
    return privilegedAccounts;
  }

  // NOTE: Mask decoding can also be handled using a variant of the coin-change DP algo.
  maskDecoder(masks: number[], targetMask: number, account: any, maskMap: any, partial: number[]) {
    let result = 0;
    partial.forEach(mask => {
      result = result | mask;
    });
    if (result === targetMask) {
      partial.forEach(partMask => {
        const findIndex = maskMap.masks.findIndex(mask => mask === partMask);
        account[maskMap.permissions[findIndex]] = true;
      });
      return;
    }
    if (result > targetMask) {
      return;
    }
    for (let i = 0 ; i < masks.length; i++) {
      const mask = masks[i];
      const remaining = [];
      for (let j = i + 1; j < masks.length; j++) {
        remaining.push(masks[j]);
      }
      const partialMasks = [...partial];
      partialMasks.push(mask);
      this.maskDecoder(remaining, targetMask, account, maskMap, partialMasks);
    }
  }

  encodePermissionsToMasks(privilegedAccounts: any, maskMap: any) {
    privilegedAccounts.forEach(account => {
      // get permissions.
      const permissions = {
        read: account.read || false,
        create: account.create || false,
        update: account.update || false,
        delete: account.delete || false,
        execute: account.execute || false
      };
      // get mask values.
      const truePermissionMasks = [];
      for (let i = 0 ; i < maskMap.permissions.length; i++) {
        if (permissions[maskMap.permissions[i]]) {
          truePermissionMasks.push(maskMap.masks[i]);
        }
      }
      if (truePermissionMasks.length !== 0) {
        // reduce to final mask value.
        const reducer = (acc, next) => acc | next;
        const finalMask = truePermissionMasks.reduce(reducer);
        account.mask = finalMask;
      } else {
        account.mask = 0;
      }
    });
    return privilegedAccounts;
  }

  async saveAccounts(accounts: any, privilege: string, target: number | string): Promise<any> {
    const accountsToBeSaved = accounts.map(account => {
      if (account.toBeUpdated) {
        return {
          id: account.id,
          mask: account.mask
        };
      }
      return {
        account: account.account.id || account.id,
        privilege: privilege,
        target: target.toString(),
        mask: account.mask
      };
    });
    return this.context.model('Users/Permissions').save(accountsToBeSaved);
  }

  async removeAccounts(accounts: any): Promise<any> {
    const accountsToBeRemoved = accounts.map(account => {
      return {
        id: account.id,
        $state: 4
      };
    });
    return this.context.model('Users/Permissions').save(accountsToBeRemoved);
  }

  async getServices(): Promise<any> {
    return this.context.model('diagnostics/services').getItems();
  }

  private _hasPermissionURL(): string {
    return 'Users/me/hasPermission';
  }
  /**
   *
   * @param permissionObject The permission object to be validated.
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has the permission, false if they don't. If the validateAdminService option is true, returns null
   * if the service is not enabled on the server.
   */
  public async hasPermission(permissionObject: PermissionValidationObject,
    options?: PermissionValidationOptions): Promise<boolean | null>  {
        // validate model
        const isValidModel = await this._isValidModel(permissionObject.model);
        if (!isValidModel) {
          console.warn(`Provided entity type ${permissionObject.model} is invalid (not found) in the current application context. Cannot validate permissions.`);
          return null;
        }
        // if the caller wants to validate AdminService
        if (options && options.validateAdminService) {
          const hasAdminService = await this.diagnostics.hasService('AdminService');
          if (!hasAdminService) {
            return null;
          }
        }
        // get permission validation result
        const permissionValidationResult: PermissionValidationResult =
          await this.context.model(this._hasPermissionURL()).save(permissionObject);
        // and return the value (boolean)
        return permissionValidationResult.value;
  }
  /**
   *
   * @param model The target model/entity type (e.g. 'Student')
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has read permission on the specified model, false if they don't.
   * If the validateAdminService option is true, returns null if the service is not enabled on the server.
   * Please consider using the generic hasPermission function for more complex validations.
   */
  public async hasPermissionToRead(model: string, options?: PermissionValidationOptions): Promise<boolean | null> {
    const permissionValidationObject: PermissionValidationObject = {
      model: model,
      mask: this.permissionMasks.READ
    };
    const permissionValidationResult: boolean = await this.hasPermission(permissionValidationObject, options);
    return permissionValidationResult;
  }
  /**
   *
   * @param model The target model/entity type (e.g. 'Student')
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has create permission on the specified model, false if they don't.
   * If the validateAdminService option is true, returns null if the service is not enabled on the server.
   * Please consider using the generic hasPermission function for more complex validations.
   */
  public async hasPermissionToCreate(model: string, options?: PermissionValidationOptions): Promise<boolean | null> {
    const permissionValidationObject: PermissionValidationObject = {
      model: model,
      mask: this.permissionMasks.CREATE
    };
    const permissionValidationResult: boolean = await this.hasPermission(permissionValidationObject, options);
    return permissionValidationResult;
  }
  /**
   *
   * @param model The target model/entity type (e.g. 'Student')
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has update permission on the specified model, false if they don't.
   * If the validateAdminService option is true, returns null if the service is not enabled on the server.
   * Please consider using the generic hasPermission function for more complex validations.
   */
  public async hasPermissionToUpdate(model: string, options?: PermissionValidationOptions): Promise<boolean | null> {
    const permissionValidationObject: PermissionValidationObject = {
      model: model,
      mask: this.permissionMasks.UPDATE
    };
    const permissionValidationResult: boolean = await this.hasPermission(permissionValidationObject, options);
    return permissionValidationResult;
  }
  /**
   *
   * @param model The target model/entity type (e.g. 'Student')
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has delete permission on the specified model, false if they don't.
   * If the validateAdminService option is true, returns null if the service is not enabled on the server.
   * Please consider using the generic hasPermission function for more complex validations.
  */
  public async hasPermissionToDelete(model: string, options?: PermissionValidationOptions): Promise<boolean | null> {
    const permissionValidationObject: PermissionValidationObject = {
      model: model,
      mask: this.permissionMasks.DELETE
    };
    const permissionValidationResult: boolean = await this.hasPermission(permissionValidationObject, options);
    return permissionValidationResult;
  }
  /**
   *
   * @param model The target model/entity type (e.g. 'Student')
   * @param options Extra options for the function, like AdminService validation.
   * @returns True if the user has execute permission on the specified model, false if they don't.
   * If the validateAdminService option is true, returns null if the service is not enabled on the server.
   * Please consider using the generic hasPermission function for more specific execute permission validations.
  */
  public async hasPermissionToExecute(model: string, options?: PermissionValidationOptions): Promise<boolean | null> {
    const permissionValidationObject: PermissionValidationObject = {
      model: model,
      mask: this.permissionMasks.EXECUTE
    };
    const permissionValidationResult: boolean = await this.hasPermission(permissionValidationObject, options);
    return permissionValidationResult;
  }

  private async _isValidModel(model: string): Promise<boolean> {
    // validate model by entity type
    const schema = await this.context.getMetadata();
    const testName = new RegExp(`^${model}$`, 'i');
    // find entity set
    const findEntityType = schema.EntityType.find((x) => {
      return testName.test(x.Name);
    });
    return !!findEntityType;
  }
}
