import { TemplatePipe } from '@universis/common';
import { AdvancedColumnFormatter } from '@universis/ngx-tables';

export class CheckBoxFormatter extends AdvancedColumnFormatter {
  render(data, type, row, meta) {
      // get column
      const column = meta.settings.aoColumns[meta.col];
      if (column && column.data) {
        if (data) {
          const _tpl = `<input class="checkbox checkbox-theme permissions" type="checkbox" checked
          id="s-${meta.col}-${meta.row}" /><label for="s-${meta.col}-${meta.row}">&nbsp;</label>`;
          return  new TemplatePipe().transform(_tpl, row);
        } else {
          const _tpl = `<input class="checkbox checkbox-theme permissions" type="checkbox"
          id="s-${meta.col}-${meta.row}" /><label for="s-${meta.col}-${meta.row}">&nbsp;</label>`;
          return  new TemplatePipe().transform(_tpl, row);
        }
      }
      return data;
  }
  constructor() {
      super();
  }
}
