import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// tslint:disable-next-line:max-line-length
import { AdvancedSelectService, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, AdvancedTableEditorDirective, COLUMN_FORMATTERS } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { AdminService } from '../admin.service';
import {AUTHORIZED_ACCOUNTS_CONFIG} from './authorized-accounts.config';
import {ACCOUNTS_CONFIG} from './accounts.config';
import { CheckBoxFormatter } from './checkbox-formatter';
import { IAdvancedTableFormatters } from '@universis/ngx-tables';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService, LoadingService} from '@universis/common';
import { AngularDataContext } from '@themost/angular';



@Component({
  selector: 'lib-permissions',
  templateUrl: './permissions.component.html'
})
export class PermissionsComponent implements OnInit, OnDestroy {

  public modelName: string;
  public target: number | string;
  public permissionsList: string[];
  public config: AdvancedTableConfiguration;
  @ViewChild('accounts') accounts: AdvancedTableComponent;
  @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
  private dataSubscription: Subscription;
  private subscription: Subscription;
  public privilegedAccounts: any[];
  public recordsTotal: any;
  public readonly accountsConfig = ACCOUNTS_CONFIG;
  public initialAccounts: any[];
  public isAvailable = true;
  public isLoading = true;
  public targetDescription: string;
  public maskMap = {
    permissions: ['read', 'create', 'update', 'delete', 'execute'],
    masks: [1, 2, 4, 8, 16]
  };
  constructor(private _activatedRoute: ActivatedRoute,
              private _adminService: AdminService,
              private _selectService: AdvancedSelectService,
              @Inject(COLUMN_FORMATTERS) private _columnFormatters: IAdvancedTableFormatters,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.isAvailable = await this.checkAvailability();
    if (this.isAvailable) {
      this.subscription = this._activatedRoute.params.subscribe(params => {
        if (params.target) {
          this.target = params.target;
        } else {
          // if target is not present in the route, use 0.
          this.target = 0;
        }
        this.dataSubscription = this._activatedRoute.data.subscribe(async data => {
          if (data.model) {
            this.modelName = data.model;
            if (this.target !== 0) {
              // get schema.
              const schema = await this._context.getMetadata();
              // get entitySet.
              const entitySet = schema.EntityContainer.EntitySet.find(element => {
                return element.EntityType === this.modelName;
              });
              // get model.
              const model = entitySet.Name;
              // get target.
              const target = await this._context.model(model).where('id').equal(this.target).select('id, name').getItem();
              // set targetDescription.
              this.targetDescription = target.name;
              if (typeof this.targetDescription === 'undefined' || this.targetDescription === null) {
                // TODO (api service).
              }
            } else {
              // use all elements description.
              this.targetDescription = this._translateService.instant('Admin.Permissions.UniversalTarget');
            }
          } else {
            console.log('Model should be defined');
          }
          if (data.permissions) {
            this.permissionsList = data.permissions;
          } else {
            // if permissionsList is not present in the route, use default one.
            this.permissionsList = ['read', 'create', 'update', 'delete', 'execute'];
          }
          // get relevant accounts.
          try {
            this.privilegedAccounts = await this._adminService.getPrivilegedAccounts(this.modelName, this.target);
          } catch (err) {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
          // decode masks.
          // tslint:disable-next-line:max-line-length
          this.privilegedAccounts = this._adminService.decodeMasksToPermissions(this.privilegedAccounts, this.maskMap, this.permissionsList);
          // deep copy initial state (for undo).
          this.initialAccounts = JSON.parse(JSON.stringify(this.privilegedAccounts));
          this.config = cloneDeep(AUTHORIZED_ACCOUNTS_CONFIG);
          // set config.
          this.accounts.config = AdvancedTableConfiguration.cast(this.config);
          // add CheckBoxFormatter.
          this.extendFormatters();
          // set permission columns for table.
          const permissionColumns = [];
          this.permissionsList.forEach(permission => {
            const permissionTitle = this._translateService.instant(`Admin.Permissions.Types.${permission}`);
            permissionColumns.push({
              name: permission,
              title: permissionTitle,
              property: permission,
              formatter: 'CheckBoxFormatter'
            });
          });
          // push columns.
          this.accounts.config.columns.push(...permissionColumns);
          this.accounts.ngOnInit();
          // set items.
          this.tableEditor.set(this.privilegedAccounts);
          this._loadingService.hideLoading();
          this.isLoading = false;
          // listen for changes on permissions.
          this.accounts.dataTable.on('change', '.permissions', (change) => {
            // split id.
            const idSplit = change.target.id.replace('s', '').split('-');
            // get account.
            const account = idSplit[2];
            // get permission.
            const permission = idSplit[1];
            // get permissionName.
            const permissionName = this.permissionsList[permission - 6];
            // get value.
            const value = change.target.checked;
            // set value.
            this.privilegedAccounts[account][permissionName] = value;
            // set table editor as dirty.
            this.tableEditor.dirty = true;
          });
          this.privilegedAccounts.forEach(account => {
            // used to update existing account masks.
            account.toBeUpdated = true;
          });
        });
      });
    } else {
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  add() {
    this._selectService.select({
      modalTitle: this._translateService.instant('Admin.Permissions.AddAccount'),
      tableConfig: this.accountsConfig
    }).then((result) => {
      if (result.result === 'ok') {
        let items = result.items;
        items.forEach(item => {
          // check for any already present items.
          const findItem = this.privilegedAccounts.findIndex(account => account.account.name === item.name);
          if (findItem > -1) {
            const index = items.indexOf(item);
            items.splice(index, 1);
          }
        });
        // check if read is present in permissions list.
        const findIndex = this.permissionsList.findIndex(permission => permission === 'read');
        if (findIndex > -1 ) {
          // set mask to 1 (read).
          items.forEach(item => {
            item.mask = 1;
            // sync with authorized-accounts config.
            Object.defineProperty(item, 'account', {
              enumerable: true,
              writable: true,
              value: {
                name: item.name,
                additionalType: item.additionalType
              }
            });
          });
        } else {
          // set mask to 16 (execute).
          items.forEach(item => {
            item.mask = 16;
            // sync with authorized-accounts config.
            Object.defineProperty(item, 'account', {
              enumerable: true,
              writable: true,
              value: {
                name: item.name,
                additionalType: item.additionalType
              }
            });
          });
        }
        if (items.length !== 0) {
          // decode masks.
          items = this._adminService.decodeMasksToPermissions(items, this.maskMap, this.permissionsList);
          // add items.
          this.tableEditor.add(...items);
          // keep the sync on main array (to handle checkbox checking events).
          this.privilegedAccounts = this.privilegedAccounts.concat(items);
        }
      }
    });
  }

  apply() {
    this._modalService.showInfoDialog(
      this._translateService.instant('Admin.Permissions.ApplyChanges'),
      this._translateService.instant('Admin.Permissions.ApplyChangesMessage'),
      DIALOG_BUTTONS.OkCancel).then(async result => {
      if (result === 'ok') {
        // get removed items.
        const removedItems = Array.from(this.accounts.dataTable.rows('.removed').data());
        removedItems.forEach(account => {
            const findIndex = this.privilegedAccounts.findIndex(acc => acc.account.name === account['account'].name);
            if (findIndex > -1) {
              this.privilegedAccounts.splice(findIndex, 1);
            }
        });
        if (removedItems.length !== 0) {
          // remove items.
          try {
            await this._adminService.removeAccounts(removedItems);
          } catch (err) {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
        }
        if (this.privilegedAccounts.length !== 0) {
          // encode masks.
          this.privilegedAccounts = this._adminService.encodePermissionsToMasks(this.privilegedAccounts, this.maskMap);
          // save items.
          try {
            await this._adminService.saveAccounts(this.privilegedAccounts, this.modelName, this.target);
            this._toastService.show(this._translateService.instant('Admin.Permissions.ApplyChanges'),
            this._translateService.instant('Admin.Permissions.ApplyChangesToastMessage'));
          } catch (err) {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          }
        }
        // fetch items.
        this.fetchItems();
        this.tableEditor.dirty = false;
      }
    });
  }

  remove() {
    // keep the sync on main array (to handle checkbox checking events).
    this.accounts.selected.forEach(account => {
      if (!account.hasOwnProperty('toBeUpdated')) {
        const findIndex = this.privilegedAccounts.findIndex(acc => acc.account.name === account.account.name);
        if (findIndex > -1) {
          this.privilegedAccounts.splice(findIndex, 1);
        }
      }
    });
    // remove items.
    this.tableEditor.remove(...this.accounts.selected);
  }

  async fetchItems() {
    // get relevant accounts.
    try {
      this.privilegedAccounts = await this._adminService.getPrivilegedAccounts(this.modelName, this.target);
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
    // decode masks.
    this.privilegedAccounts = this._adminService.decodeMasksToPermissions(this.privilegedAccounts, this.maskMap, this.permissionsList);
    // reset initial state.
    this.initialAccounts = JSON.parse(JSON.stringify(this.privilegedAccounts));
    this.privilegedAccounts.forEach(account => {
      // used to update existing account masks.
      account.toBeUpdated = true;
    });
    // set items.
    this.tableEditor.set(this.privilegedAccounts);
  }

  undo() {
    this._modalService.showInfoDialog(
      this._translateService.instant('Admin.Permissions.UndoChanges'),
      this._translateService.instant('Admin.Permissions.UndoChangesMessage'),
      DIALOG_BUTTONS.OkCancel).then(result => {
      if (result === 'ok') {
        // undo.
        this.tableEditor.undo();
        // rollback to initial checkbox states for existing accounts.
        this.tableEditor.set(this.initialAccounts);
        // reset accounts.
        this.privilegedAccounts = JSON.parse(JSON.stringify(this.initialAccounts));
        this.privilegedAccounts.forEach(account => {
          // used to update existing account masks.
          account.toBeUpdated = true;
        });
      }
    });
  }

  extendFormatters() {
    // add checkbox formatter proto to existing formatters.
    Object.defineProperty(this._columnFormatters, 'CheckBoxFormatter', {
      value: CheckBoxFormatter.prototype
    });
  }

  checkAvailability(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this._adminService.getServices().then(diagonsticServices => {
        const services = diagonsticServices;
        // check if admin service is running.
        const adminService = services.find(service => service.serviceType === 'AdminService');
        if (adminService) {
          return resolve(true);
        } else {
          return resolve(false);
        }
      }).catch(err => {
        return reject(err);
      });
    });
  }
}
