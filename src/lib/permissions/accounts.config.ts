/* tslint:disable quotemark */
export const ACCOUNTS_CONFIG = {
    "title": "Accounts",
    "model": "Accounts",
    "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
    "selectable": true,
    "multipleSelect": true,
    "columns": [
      {
        "name": "name",
        "property": "name",
        "title": "Admin.Permissions.Name"
      },
      {
        "name": "alternateName",
        "property": "alternateName",
        "title": "Admin.Permissions.AlternateName"
      },
      {
        "name": "additionalType",
        "property": "additionalType",
        "title": "Admin.Permissions.AdditionalType"
      },
      {
        "name": "id",
        "property": "id",
        "title": "Id",
        "hidden": true
      }
    ],
    "defaults": {
    }
  };
