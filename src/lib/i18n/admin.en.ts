/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const en = {
  "Admin": {
    "Users": {
      "Title": "User",
      "TitlePlural": "Users",
      "Enabled": {
        "true": "Active",
        "false": "Inactive",
        "1": "Active",
        "0": "Inactive",
        "null": ""
      },
      "AlternateName" : "AlternateName",
      "Name" : "Name",
      "Description" : "Description",
      "LastLogon" : "Last Logon",
      "StatusTitle" : "Status",
      "Preview": "Preview",
      "Edit": "Edit",
      "Id": "Id",
      "LogonCount": "Logon Count",
      "LockoutTime": "Lockout Time",
      "All": "All",
      "New": "Create",
      "NewTitle": "Create User",
      "Export": "Export",
      "Type": "Type",
      "View": "View",
      "Empty": "There are no records",
      "Total": "total",
      "Info": "Info",
      "Add": "Add user",
      "Remove": "Remove user",
      "SelectUsers": "Select Users",
      "RemoveDepartments" : {
        "Title": "Remove department",
        "Message": "You are about to remove selected departments from this user. Do you want to continue?",
        "One": "Department successfully removed",
        "Many": "{{value}} departments successfully removed."
      },
      "RemoveGroups" : {
        "Title": "Remove group",
        "Message": "You are about to remove selected groups from this user. Do you want to continue?",
        "One": "Group successfully removed",
        "Many": "{{value}} groups successfully removed."
      },
      "CreateUser": "Create user"
    },
    "Groups": {
      "Title" : "Group",
      "TitlePlural": "Groups",
      "AlternateName" : "Alternate Name",
      "Name" : "Name",
      "Edit": "Edit",
      "New": "Create",
      "NewTitle": "Create Group",
      "Add": "Add Group",
      "SelectGroups": "Select Groups",
      "Remove": "Remove",
      "AddGroups" : {
        "Title": "Add group to user",
        "AllAlreadyExist": "the group(s) you selected already belong(s) to the user.",
        "One": "A new group successfully added to the user.",
        "Many": "{{value}} groups were successfully added to the user.",
        "None": "No group selected for add.",
      },
      "AddUsers": {
        "Title": "Add user to group",
        "AllAlreadyExist": "The user(s) you selected already belong(s) to the group.",
        "One": "One user successfully added.",
        "Many": "{{value}} users successfully added"
      },
      "RemoveUsers" : {
        "Title": "Remove users",
        "Message": "You are about to remove selected users from group. Do you want to continue?",
        "One": "One user successfully removed.",
        "Many": "{{value}} users successfully removed."
      },
    },
    "Departments": {
      "Title" : "Departments",
      "AlternativeCode" : "Alternative Code",
      "Name" : "Name",
      "City" : "City",
      "Add": "Add department",
      "Remove": "Remove department",
      "SelectDepartments": "Select department",
      "AddDepartments" : {
        "Title": "Add department to user",
        "AllAlreadyExist": "This user already belong(s) to selected department(s).",
        "One": "Department successfully added.",
        "Many": "{{value}} departments successfully added."
      }
    },
    "Permissions": {
      "EditPermissions": "Edit Permissions",
      "Accounts": "Authorized accounts",
      "AccountSingular": "Authorized account",
      "AddAccount": "Add account",
      "RemoveAccount": "Remove account",
      "RemoveAccounts": "Remove accounts",
      "UndoChanges": "Undo changes",
      "UndoChangesMessage": "You are about to undo the current changes. Do you wish to proceed?",
      "ApplyChanges": "Apply changes",
      "ApplyChangesMessage": "You are about to apply the current permission changes. Do you wish to proceed?",
      "ApplyChangesToastMessage": "The permission changes were saved successfully.",
      "Name": "Account name",
      "AdditionalType": "Account type",
      "AlternateName": "AlternateName",
      "Types": {
        "read": "Read",
        "create": "Create",
        "update": "Update",
        "delete": "Delete",
        "execute": "Execute"
      },
      "FeatureUnavailable": "Permission editing is not available in the version you are using.",
      "UniversalTarget": "All elements",
      "Add": "Add Departments",
      "SelectDepartments": "Select Departments",
      "AddDepartments" : {
        "Title": "Add department to user",
        "AllAlreadyExist": "the department(s) you selected already belong(s) to the user.",
        "One": "A new department successfully added to the user.",
        "Many": "{{value}} departments were successfully added to the user.",
        "None": "No department selected for add.",
      },
    }
  }
};
